import math
import numpy as np
import matplotlib.pyplot as plt


t_exp = []
T_exp = []
T_C_to_K = 273.15

with open("T_exp") as f_in:
    line = f_in.readline()
    for line in f_in:
        line = line.split();
        t_exp.append(float(line[1]));
        # line[5] - Ts_exp on surface, line[3] - Ts_exp in bed middle
        T_exp.append(float(line[3]) + T_C_to_K);



relax_solid = 1 #0.3
iter_tol_lim = 1e-5

SMALL = 1e-10

R0 = 8.314

alfa_s_0 = 0.029
rho_s = 600.0 # [kg/m^3]
C_s = 1590.0  # [J/kg K]

Ys_char_0 = 0.0
Ys_dw_0 = 1.0
p_a = 1e5
R = 287.0
Y_o2 = 0.1 #233 #0.233

ks_pyr = 5.01e10    # [s^-1]
Es_pyr = 144.6e3    # [J/mol]

ks_char = 4e3 #6e7 #1.2e4     # [m/s]
Es_char = 140.65e3  # [J/mol]

nus_ash = 0.038
nus_char = 0.342
nus_soot = 0.0
nus_o2 = 1.65

M_C = 12.0
M_O2 = 32.0

sigma_s = 5300.0

dt = 0.1           # [s]
t = []
Ts = []
alfa_s = []
Ys_dw = []
Ys_char = []


t.append(t_exp[0])
Ts.append(T_exp[0])
alfa_s.append(alfa_s_0)
Ys_dw.append(Ys_dw_0)
Ys_char.append(Ys_char_0)


with open("out","w+") as f_out:
    f_out.write("    t               T           alfa_s         Ys_dw       Ys_char   \n")

    m = rho_s*alfa_s[0]
    m_dw = rho_s*alfa_s[0]*Ys_dw[0]
    m_char = rho_s*alfa_s[0]*Ys_char[0]
    m_ash = rho_s*alfa_s[0]*(1 - Ys_dw[0] - Ys_char[0])

    i_exp = 0
    i = 0

    # Time stepping
    while i_exp < len(t_exp)-2:
        i += 1
        t.append(t[i-1]+dt)

        if(t[i] > t_exp[i_exp+1]): i_exp += 1

        alfa_s.append(alfa_s[i-1])
        Ys_char.append(Ys_char[i-1])
        Ys_dw.append(Ys_dw[i-1])

        Ts.append((T_exp[i_exp+1]-T_exp[i_exp])/(t_exp[i_exp+1]-t_exp[i_exp])*(t[i]-t_exp[i_exp]) + T_exp[i_exp])

        rho_g = p_a/(R*Ts[i])

        iter_tol = iter_tol_lim*10
        iter_count = 0
        
        while(iter_tol > iter_tol_lim):
            iter_count += 1

            ws_pyr = alfa_s[i]*rho_s*ks_pyr*Ys_dw[i]*np.exp(-Es_pyr/(R0*Ts[i]))
            w_pyr_max1 = Ys_dw[i-1]*alfa_s[i-1]*rho_s/dt
            ws_pyr = min(ws_pyr,w_pyr_max1)
            
            # ws_char = 1.0/nus_o2*ks_char*alfa_s[i]*sigma_s*(1-alfa_s[i])*rho_g*Y_o2*np.exp(-Es_char/(R0*Ts[i]))
            ws_char = 1.0/nus_o2*ks_char*alfa_s[i]*sigma_s*rho_s*np.exp(-Es_char/(R0*Ts[i]))
            w_char_max1 = nus_char*ws_pyr + Ys_char[i-1]*alfa_s[i-1]*rho_s/dt
            w_char_max2 = alfa_s[i]*rho_s/dt
            ws_char = min(ws_char,w_char_max1)
            ws_char = min(ws_char,w_char_max2)

            m_dot = (1-nus_char)*ws_pyr + (1-nus_ash)*ws_char

            alfa_s_star = alfa_s[i-1] - dt*m_dot/rho_s
            Ys_dw_star = 1.0/alfa_s_star*(alfa_s[i-1]*Ys_dw[i-1] - dt*ws_pyr/rho_s)
            Ys_char_star = 1.0/alfa_s_star*(alfa_s[i-1]*Ys_char[i-1] + dt*(nus_char*ws_pyr - ws_char)/rho_s)

            iter_tol = abs(Ys_dw_star-Ys_dw[i]) + abs(Ys_char_star-Ys_char[i])

            alfa_s_star  = alfa_s[i]  + relax_solid*(alfa_s_star - alfa_s[i])
            Ys_dw_star   = Ys_dw[i]   + relax_solid*(Ys_dw_star - Ys_dw[i])
            Ys_char_star = Ys_char[i] + relax_solid*(Ys_char_star - Ys_char[i])
            
            # Update the solid fields due to iterations inside a time step
            alfa_s[i] = alfa_s_star
            Ys_char[i] = Ys_char_star
            Ys_dw[i] = Ys_dw_star

        m = rho_s*alfa_s[i]
        m_dw = rho_s*alfa_s[i]*Ys_dw[i]
        m_char = rho_s*alfa_s[i]*Ys_char[i]
        m_ash = rho_s*alfa_s[i]*(1 - Ys_dw[i] - Ys_char[i])
            

#        f_out.write('{:5.1e}'.format(t[i]) + '{:16.5e}'.format(Ts[i]) + '{:16.5e}'.format(alfa_s[i]) + '{:16.5e}'.format(Ys_dw[i]) + '{:16.5e}'.format(Ys_char[i]) + '\n')

        f_out.write('{:5.1e}'.format(t[i]) + '{:16.5e}'.format(m) + '{:16.5e}'.format(m_dw) + '{:16.5e}'.format(m_char) + '{:16.5e}'.format(m_ash) + '\n')

#l_T, = plt.plot(t,Ts)
l_alfa_s, = plt.plot(t,alfa_s)
l_Ys_dw, = plt.plot(t,Ys_dw)
l_Ys_char, = plt.plot(t,Ys_char)

plt.legend((l_alfa_s,l_Ys_dw,l_Ys_char),
           ("alfa_s","Ys_dw","Ys_char"),
           loc="center left")
#plt.plot(t_exp,T_exp)
plt.show()
