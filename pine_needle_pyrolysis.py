import math
import numpy as np
import matplotlib.pyplot as plt

SMALL = 1e-10

p = 1e5
R = 287.0

alfa_s_0 = 0.029
rho_s_0 = 710 # [kg/m^3]
C_s = 1470  # [J/kg K]
T0 = 300    # [K]
T_end = 900.0 # [K]
dTdt = 50   # [K/s]

Y_h2o_0 = 0.1
Y_char_0 = 0
Y_dw_0 = 0.9
Y_ash_0 = 0
Y_o2 = 0.233

k_h2o = 6e5
E_h2o = 5800.0

k_pyr = 3.64e4
E_pyr = 7250.0

k_char = 430.0
E_char = 5700 #9000.0

nu_ash = 0.2
nu_char = 0.338
nu_soot = 0.05
nu_o2 = 1.65

M_C = 12.0
M_O2 = 32.0

sigma_s = 4550.0

dt = 0.01 # [s]
t_end = (T_end - T0)/dTdt
Nt = int(t_end/dt)

t = np.zeros(Nt+1)
T = np.zeros(Nt+1)
Y_h2o = np.zeros(Nt+1)
Y_char = np.zeros(Nt+1)
Y_dw = np.zeros(Nt+1)
rho_s = np.zeros(Nt+1)
alfa_s = np.zeros(Nt+1)

T[0] = T0
Y_h2o[0] = Y_h2o_0
Y_char[0] = Y_char_0
Y_dw[0] = Y_dw_0
alfa_s[0] = alfa_s_0
rho_s[0] = rho_s_0

with open("out","w+") as f_out:
    f_out.write("    t         T            Y_dw          Y_char           Y_h2o        alfa_s        rho_s     \n")
        
    for i in range(1,Nt+1):
        t[i] = t[i-1] + dt
        T[i] = T[i-1] + dTdt*dt

        w_h2o = alfa_s[i-1]*rho_s[i-1]*k_h2o*T[i-1]**(-0.5)*Y_h2o[i-1]*np.exp(-E_h2o/T[i-1])
        w_pyr = alfa_s[i-1]*rho_s[i-1]*k_pyr*Y_dw[i-1]*np.exp(-E_pyr/T[i-1])
        w_char = 1.0/nu_o2*k_char*alfa_s[i-1]*sigma_s*p/(R*T[i-1])*Y_o2*np.exp(-E_char/T[i-1])

        if(alfa_s[i-1] < SMALL): w_char = 0
        if(Y_char[i-1] < SMALL and nu_char*(1-nu_soot)*w_pyr - w_char < 0): w_char = 0
        

        alfa_s[i] = alfa_s[i-1] - 1.0/rho_s[i-1]*dt*w_char
        rho_s[i] = 1.0/(alfa_s[i])*(alfa_s[i-1]*rho_s[i-1] - dt*((1-nu_char+nu_char*nu_soot)*w_pyr + w_h2o + (1-nu_ash)*w_char))
        Y_dw[i] = 1.0/(alfa_s[i]*rho_s[i])*(Y_dw[i-1]*alfa_s[i-1]*rho_s[i-1] - dt*w_pyr)
        Y_h2o[i] = 1.0/(alfa_s[i]*rho_s[i])*(Y_h2o[i-1]*alfa_s[i-1]*rho_s[i-1] - dt*w_h2o)
        Y_char[i] = 1.0/(alfa_s[i]*rho_s[i])*(Y_char[i-1]*alfa_s[i-1]*rho_s[i-1] + dt*(nu_char*(1-nu_soot)*w_pyr - w_char))
        
        f_out.write('{:5.1e}'.format(t[i]) + '{:16.5e}'.format(T[i]) + '{:16.5e}'.format(Y_dw[i]) + '{:16.5e}'.format(Y_char[i]) + '{:16.5e}'.format(Y_h2o[i]) + '{:16.5e}'.format(alfa_s[i]) + '{:16.5e}'.format(rho_s[i]) + '\n')


l_Y_char, = plt.plot(T,Y_char)
l_Y_dw, = plt.plot(T,Y_dw)
l_Y_h2o, = plt.plot(T,Y_h2o)
l_Y_ash, = plt.plot(T,1-Y_char-Y_dw-Y_h2o)
l_dM, = plt.plot(T,1-alfa_s*rho_s/(alfa_s[0]*rho_s[0]))
l_alfa_s, = plt.plot(T,alfa_s)

plt.legend((l_Y_char,l_Y_dw,l_Y_h2o,l_Y_ash,l_dM,l_alfa_s),
           ("Y_char","Y_dw","Y_h2o","Y_ash","dM","alfa_s"),
           loc="center left")

plt.show()
